# GameName

<img src="https://github.com/nazarkohut/readme_logos/blob/main/TeamNameLogo.svg?raw=true" width="35"/> Made by TeamName

## Lore
### Once upon a time, there was a wealthy village with friendly, kind, and hard-working people. These lands had fertile soils, precious minerals, and rich foliage and fauna, so people started selling their materials. It was good at first, but after a visit from some greedy trader, things changed a lot(he wanted to buy everything they had at a ridiculous price, of course, his proposal was rejected). After being banished(as he began to poison animals and destroy plants), the earth was covered with darkness, and dark powers summoned by a hired necromancer started destroying houses and killing people. But we all know that good always triumphs over evil.

## Game
### Your task is to bring peace back to the lands and to cleanse the land of evil forces.

## How to download and launch?
### Go to [Google drive build link](https://drive.google.com/drive/folders/13cIH4JUFFhpUiy3tygcgqDRUVSOdzxcq?usp=sharing) and search for .exe file.


##### TeamName team:
###### [Vasya Yovbak](https://gitlab.com/Vasyl_Yovbak)
###### [Nazar Kohut](https://gitlab.com/nazarkohut)
###### [Nazarii Hryhorash](https://gitlab.com/Parsleyka)
###### [Oleksandr Patsuryn](https://gitlab.com/Oleksandr_Patsuryn)
###### [Oleksandr Yovbak](https://gitlab.com/alexxyyoo)
###### [Denys Kostiuk](https://gitlab.com/Laso4ka)

###### Developed with Unreal Engine 5
